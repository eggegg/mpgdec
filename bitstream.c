#include "bitstream.h"

#define ALIGNED(p) ((p) & 7)

void stmInit(stream* stm, FILE* fp) {
  stm->source = fp;
  stm->pt = stm->buffer;
  stm->bitbuffer = 0;
  stm->bitpt = 0;
  fread(stm->buffer, 1, BUFFER_SIZE, fp);
}

static inline void stmFillBuffer(stream* stm) {
  if (stm->pt == stm->buffer + BUFFER_SIZE) {
    fread(stm->buffer, 1, BUFFER_SIZE, stm->source);
    stm->pt = stm->buffer;
  }
}

static inline void stmFillBitBuffer(stream* stm) {
  stmFillBuffer(stm);
  stm->bitpt += 8;
  stm->bitbuffer = (stm->bitbuffer << 8) | *stm->pt;
  stm->pt++;
}

uint32_t stmPeep32(stream* stm, uint8_t bits) {
  while (stm->bitpt < bits) {
    stmFillBitBuffer(stm);
  }
  if (bits == 32) {
    return stm->bitbuffer;
  } else {
    return (stm->bitbuffer >> (stm->bitpt - bits)) & ((1 << bits) - 1);
  }
}

static inline void stmSkipBits(stream* stm, uint8_t bits) {
  if (stm->bitpt < bits) {
    stmPeep32(stm, bits);
  }
  stm->bitpt -= bits;
}

uint32_t stmGet32(stream* stm, uint8_t bits) {
  uint32_t ret = stmPeep32(stm, bits);
  stmSkipBits(stm, bits);
  return ret;
}

uint8_t stmAln8(stream* stm) {
  stmFillBuffer(stm);
  stm->pt++;
  return *(stm->pt - 1);
}

void stmByteAlign(stream* stm) {
  uint8_t residue = ALIGNED(stm->bitpt);
  if (residue) {
    stmSkipBits(stm, residue);
  }
}

void stmNextStartCode(stream* stm) {
  stmByteAlign(stm);
  while (stmPeep32(stm, 24) != 1) {
    stmSkipBits(stm, 8);
  }
}
