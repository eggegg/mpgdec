#include <stdint.h>
#include <stdio.h>
#include "cv.h"
#include "highgui.h"
#include "framebuf.h"

#define WINDOW_NAME "MPGDEC"
#define FIX(x) (((x) < 0) ? 0 : (((x) > 0xFF) ? 0xFF : (uint8_t) (x)))
#define CHANNEL(i, c, x, y) ((i)->imageData[(y)*(i)->widthStep+(x)*(i)->nChannels+(c)])
#define BLUE(i, x, y) CHANNEL(i, 0, x, y)
#define GREEN(i, x, y) CHANNEL(i, 1, x, y)
#define RED(i, x, y) CHANNEL(i, 2, x, y)

static IplImage* bufImage;
static frame_buffer* playQueue = NULL;
static uint32_t nowPlay;

static inline frame_buffer* searchQueue() {
  frame_buffer* fb = playQueue;
  frame_buffer* prev = NULL;
  while (fb != NULL) {
    if (fb->order == nowPlay) {
      if (prev) {
        prev->queue_next = fb->queue_next;
      } else {
        playQueue = fb->queue_next;
      }
      return fb;
    }
    prev = fb;
    fb = fb->queue_next;
  }
  return NULL;
}

void dspInit(uint16_t width, uint16_t height) {
  CvSize size = cvSize(width, height);
  bufImage = cvCreateImage(size, IPL_DEPTH_8U, 3);
  cvNamedWindow(WINDOW_NAME, 1);
}

void dspClearGop() {
  nowPlay = 0;
  playQueue = NULL;
}

void dspShow(frame_buffer* fb) {
  if (fb->order != nowPlay) {
    fb->queue_next = playQueue;
    playQueue = fb;
    fb = searchQueue();
    if (!fb) {
      return;
    }
  }

  do {
    fprintf(stderr, "showing: %d\n", nowPlay);
    ++nowPlay;

    uint16_t i, j, t;
    int32_t cy, cb, cr;
    for (j = 0; j < bufImage->height; ++j) {
      for (i = 0; i < bufImage->width; ++i) {
        cy = fb->cy[j * bufImage->width + i] << 8;

        t = (j >> 1) * (bufImage->width >> 1) + (i >> 1);
        cb = fb->cb[t] - 128;
        cr = fb->cr[t] - 128;

        RED(bufImage, i, j)   = FIX((cy            + 359 * cr + 128) >> 8);
        GREEN(bufImage, i, j) = FIX((cy -  88 * cb - 183 * cr + 128) >> 8);
        BLUE(bufImage, i, j)  = FIX((cy + 454 * cb            + 128) >> 8);
        // RED(bufImage, i, j) = fb->cy[j * bufImage->width + i];
        // GREEN(bufImage, i, j) = fb->cy[j * bufImage->width + i];
        // BLUE(bufImage, i, j) = fb->cy[j * bufImage->width + i];
      }
    }
    cvShowImage(WINDOW_NAME, bufImage);
    cvWaitKey(1000/30);
    // cvWaitKey(0);
    // frmFreeFrame(fb);
  } while (fb = searchQueue());
}

void dspFinal() {
  cvDestroyWindow(WINDOW_NAME);
  cvReleaseImage(&bufImage);
}
