
CC = gcc
CFLAGS = -O3 `pkg-config --cflags opencv`
PKG = `pkg-config opencv --libs`

mpgdec: main.o bitstream.o header.o decode.o framebuf.o display.o
	$(CC) $(CFLAGS) $(PKG) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $^

.PHONY: clean

clean:
	rm *.o mpgdec
