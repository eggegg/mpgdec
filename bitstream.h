#ifndef __BITSTREAM_H__
#define __BITSTREAM_H__

#include <stdint.h>
#include <stdio.h>

#define BUFFER_SIZE 1048576

typedef struct _stream_t {
  FILE* source;
  uint8_t buffer[BUFFER_SIZE];
  uint8_t* pt;
  uint32_t bitbuffer;
  uint8_t bitpt;
} stream;

void stmInit(stream*, FILE*);
uint8_t stmAln8(stream*);
uint32_t stmGet32(stream*, uint8_t);
uint32_t stmPeep32(stream*, uint8_t);
void stmByteAlign(stream*);
void stmNextStartCode(stream*);

#endif
