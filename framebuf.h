#ifndef __FRAMEBUF_H__
#define __FRAMEBUF_H__

#include <stdint.h>

typedef struct _frame_buffer {
  struct _frame_buffer* next;
  struct _frame_buffer* prev;
  struct _frame_buffer* queue_next;
  uint8_t* cy;
  uint8_t* cb;
  uint8_t* cr;
  uint32_t order;
} frame_buffer;

frame_buffer* frmGetFrame(uint16_t, uint16_t);
void frmFreeFrame(frame_buffer*);
void frmFreeAll();
void frmFinal();
void dspInit(uint16_t, uint16_t);
void dspClearGop();
void dspShow(frame_buffer*);
void dspFinal();

#endif
