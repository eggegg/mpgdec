#include <stdlib.h>
#include <assert.h>
#include "mpgdec.h"
#include "vlc.h"
#include "tbl.h"

#define SIGN(x) (((x)>0)?1:(((x)<0))?-1:0)
#define CLIP(x) (((x) > 2047) ? 2047 : (((x) < -2048) ? -2048 : (x)))

static inline void cleanExtra(stream* stm) {
  uint8_t nextbit = stmGet32(stm, 1);
  while (nextbit) {
    stmGet32(stm, 9);
    nextbit = stmGet32(stm, 1);
  }
}

void seqInit(sequence_t* seq, stream* stm) {
  seq->sequence_header_code       = stmGet32(stm, 32);
  seq->horizontal_size            = stmGet32(stm, 12);
  seq->vertical_size              = stmGet32(stm, 12);
  seq->pel_aspect_ratio           = stmGet32(stm, 4);
  seq->picture_rate               = stmGet32(stm, 4);
  seq->bit_rate                   = stmGet32(stm, 18);
  seq->marker_bit                 = stmGet32(stm, 1);
  seq->vbv_buffer_size            = stmGet32(stm, 10);
  seq->constrained_parameter_flag = stmGet32(stm, 1);
}

void gopInit(gop_t* gop, stream* stm) {
  gop->group_start_code = stmGet32(stm, 32);
  gop->time_code        = stmGet32(stm, 25);
  gop->closed_gop       = stmGet32(stm, 1);
  gop->broken_link      = stmGet32(stm, 1);
  stmNextStartCode(stm);
}

void pictureInit(picture_t* pic, stream* stm) {
  pic->picture_start_code  = stmGet32(stm, 32);
  pic->temporal_reference  = stmGet32(stm, 10);
  pic->picture_coding_type = stmGet32(stm, 3);
  pic->vbv_delay           = stmGet32(stm, 16);

  if (pic->picture_coding_type == PFRAME
    || pic->picture_coding_type == BFRAME) {
    pic->full_pel_forward_vector = stmGet32(stm, 1);
    pic->forward_f_code          = stmGet32(stm, 3);
    pic->forward_r_size          = pic->forward_f_code - 1;
    pic->forward_f               = 1 << pic->forward_r_size;
  }
  if (pic->picture_coding_type == BFRAME) {
    pic->full_pel_backward_vector = stmGet32(stm, 1);
    pic->backward_f_code          = stmGet32(stm, 3);
    pic->backward_r_size          = pic->backward_f_code - 1;
    pic->backward_f               = 1 << pic->backward_r_size;
  }

  cleanExtra(stm);
  stmNextStartCode(stm);
}

void sliceInit(slice_t* slc, stream* stm) {
  slc->slice_start_code = stmGet32(stm, 32);
  slc->quantizer_scale  = stmGet32(stm, 5);

  cleanExtra(stm);
}

static inline int8_t checkMV(stream* stm) {
  if (stmPeep32(stm, 1)) {
    stmGet32(stm, 1);
    return 0;
  }
  uint16_t peep = stmPeep32(stm, 10);
  const motion_vec_t* mv;
  if (peep >= 0x30) {
    mv = MV_4 + stmPeep32(stm, 4);
  } else {
    mv = MV_10 + stmPeep32(stm, 10);
    assert(mv->delta != 0);
  }
  uint8_t delta = mv->delta + 1;
  stmGet32(stm, mv->len);
  if (stmGet32(stm, 1)) {
    delta = -delta;
  }
  return delta;
}

static inline void motionVectorInit(uint8_t full_pel_vector, uint8_t f, int8_t horizontal_code, int8_t vertical_code, uint8_t horizontal_r, uint8_t vertical_r, int32_t* recon_right_prev, int32_t* recon_down_prev, int32_t* recon_right, int32_t* recon_down) {

  int32_t complement_horizontal_r, complement_vertical_r;

  if (f == 1 || horizontal_code == 0) {
    complement_horizontal_r = 0;
  } else {
    complement_horizontal_r = f - 1 - horizontal_r;
  }
  if (f == 1 || vertical_code == 0) {
    complement_vertical_r = 0;
  } else {
    complement_vertical_r = f - 1 - vertical_r;
  }

  int32_t right_little, down_little, right_big, down_big;

  right_little = horizontal_code * f;
  if (right_little == 0) {
    right_big = 0;
  } else {
    if (right_little > 0) {
      right_little = right_little - complement_horizontal_r;
      right_big = right_little - 32 * f;
    } else {
      right_little = right_little + complement_horizontal_r;
      right_big = right_little + 32 * f;
    }
  }
  down_little = vertical_code * f;
  if (down_little == 0) {
    down_big = 0;
  } else {
    if (down_little > 0) {
      down_little = down_little - complement_vertical_r;
      down_big = down_little - 32 * f;
    } else {
      down_little = down_little + complement_vertical_r;
      down_big = down_little + 32 * f;
    }
  }

  int32_t min, max, new_vector;

  max = (16 * f) - 1;
  min = (-16 * f);
  new_vector = *recon_right_prev + right_little;
  if (new_vector <= max && new_vector >= min) {
    *recon_right = *recon_right_prev + right_little;
  } else {
    *recon_right = *recon_right_prev + right_big;
  }
  *recon_right_prev = *recon_right;
  if (full_pel_vector) {
    *recon_right <<= 1;
  }
  new_vector = *recon_down_prev + down_little;
  if (new_vector <= max && new_vector >= min) {
    *recon_down = *recon_down_prev + down_little;
  } else {
    *recon_down = *recon_down_prev + down_big;
  }
  *recon_down_prev = *recon_down;
  if (full_pel_vector) {
    *recon_down <<= 1;
  }
}

void blockMV(macroblock_t* mbk, picture_t* pic) {
  if (mbk->macroblock_motion_forward) {
    motionVectorInit(pic->full_pel_forward_vector, pic->forward_f, mbk->motion_horizontal_forward_code, mbk->motion_vertical_forward_code, mbk->motion_horizontal_forward_r, mbk->motion_vertical_forward_r, &mbk->recon_right_for_prev, &mbk->recon_down_for_prev, &mbk->recon_right_for, &mbk->recon_down_for);
  } else if (pic->picture_coding_type == PFRAME) {
    mbk->recon_right_for = mbk->recon_down_for = 0;
  }
  if (mbk->macroblock_motion_backward) {
    motionVectorInit(pic->full_pel_backward_vector, pic->backward_f, mbk->motion_horizontal_backward_code, mbk->motion_vertical_backward_code, mbk->motion_horizontal_backward_r, mbk->motion_vertical_backward_r, &mbk->recon_right_back_prev, &mbk->recon_down_back_prev, &mbk->recon_right_back, &mbk->recon_down_back);
  } else if (pic->picture_coding_type == PFRAME) {
    mbk->recon_right_back = mbk->recon_down_back = 0;
  }
}

void macroblockInit(macroblock_t* mbk, picture_t* pic, slice_t* slc, stream* stm) {
  while (stmPeep32(stm, 11) == 15) {
    stmGet32(stm, 11);
  }
  mbk->previous_macroblock_address = mbk->macroblock_address;
  while (stmPeep32(stm, 11) == 8) {
    mbk->macroblock_address += 33;
    stmGet32(stm, 11);
  }
  uint16_t peep = stmPeep32(stm, 12);
  const macroblk_addr_t* addr;
  if (peep >= 0x100) {
    addr = MBA_5 + (stmPeep32(stm, 5) - 2);
  } else if (peep >= 0x30) {
    addr = MBA_11 + (stmPeep32(stm, 11) - 0x18);
  } else {
    assert(0);
  }
  stmGet32(stm, addr->len);
  // NOTE: diff for 1
  mbk->macroblock_address_increment = addr->val + 1;
  uint32_t address = mbk->macroblock_address + mbk->macroblock_address_increment;
  mbk->macroblock_address = address;

  const macroblk_type_t* type;
  switch (pic->picture_coding_type) {
    case IFRAME:
      type = MBT_I + stmPeep32(stm, 1);
      break;
    case PFRAME:
      type = MBT_P + stmPeep32(stm, 5);
      break;
    case BFRAME:
      type = MBT_B + stmPeep32(stm, 6);
      break;
    default:
      assert(0);
  }
  uint8_t typebit = type->type;
  mbk->macroblock_type = typebit;
  stmGet32(stm, type->len);
  mbk->macroblock_quant           = (typebit & MACROBLOCK_QUANT) >> 4;
  mbk->macroblock_motion_forward  = (typebit & MACROBLOCK_MOTION_FORWARD) >> 3;
  mbk->macroblock_motion_backward = (typebit & MACROBLOCK_MOTION_BACKWARD) >> 2;
  mbk->macroblock_pattern         = (typebit & MACROBLOCK_PATTERN) >> 1;
  mbk->macroblock_intra           = (typebit & MACROBLOCK_INTRA);

  if (mbk->past_macroblock_intra) {
    mbk->past_intra_address = mbk->previous_macroblock_address;
  }
  mbk->past_macroblock_intra = mbk->macroblock_intra;
  if (mbk->macroblock_quant) {
    mbk->quantizer_scale = stmGet32(stm, 5);
  } else {
    mbk->quantizer_scale = slc->quantizer_scale;
  }
  if (mbk->macroblock_motion_forward) {
    mbk->motion_horizontal_forward_code = checkMV(stm);
    if ((pic->forward_f != 1) && (mbk->motion_horizontal_forward_code != 0)) {
      mbk->motion_horizontal_forward_r = stmGet32(stm, pic->forward_r_size);
    }
    mbk->motion_vertical_forward_code = checkMV(stm);
    if ((pic->forward_f != 1) && (mbk->motion_vertical_forward_code != 0)) {
      mbk->motion_vertical_forward_r = stmGet32(stm, pic->forward_r_size);
    }
  }
  if (mbk->macroblock_motion_backward) {
    mbk->motion_horizontal_backward_code = checkMV(stm);
    if ((pic->backward_f != 1) && (mbk->motion_horizontal_backward_code != 0)) {
      mbk->motion_horizontal_backward_r = stmGet32(stm, pic->backward_r_size);
    }
    mbk->motion_vertical_backward_code = checkMV(stm);
    if ((pic->backward_f != 1) && (mbk->motion_vertical_backward_code != 0)) {
      mbk->motion_vertical_backward_r = stmGet32(stm, pic->backward_r_size);
    }
  }
  if (mbk->macroblock_pattern) {
    const cbp_t* cbp;
    if (stmPeep32(stm, 9) >= 0x40) {
      cbp = CBP_7 + stmPeep32(stm, 7) - 16;
    } else {
      cbp = CBP_9 + stmPeep32(stm, 9);
    }
    stmGet32(stm, cbp->len);
    uint8_t i;
    mbk->coded_block_pattern = cbp->cbp;
    for (i = 0; i < 6; ++i) {
      if (cbp->cbp & (1 << i)) {
        mbk->pattern_code[i] = 1;
      } else {
        mbk->pattern_code[i] = 0;
      }
    }
  }
  if (mbk->macroblock_intra) {
    mbk->pattern_code[0] = 1;
    mbk->pattern_code[1] = 1;
    mbk->pattern_code[2] = 1;
    mbk->pattern_code[3] = 1;
    mbk->pattern_code[4] = 1;
    mbk->pattern_code[5] = 1;
  }
}

static inline const dc_coef_t* checkDClum(stream* stm) {
  const dc_coef_t* coef;
  if (stmPeep32(stm, 5) < 0x1F) {
    coef = DC_lum_5 + stmPeep32(stm, 5);
  } else {
    coef = DC_long + stmPeep32(stm, 9) - 0x1E0;
  }
  stmGet32(stm, coef->len);
  return coef;
}

static inline const dc_coef_t* checkDCchrom(stream* stm) {
  const dc_coef_t* coef;
  if (stmPeep32(stm, 5) < 0x1F) {
    coef = DC_chrom_5 + stmPeep32(stm, 5);
    stmGet32(stm, coef->len);
  } else {
    coef = DC_long + stmPeep32(stm, 10) - 0x3E0;
    stmGet32(stm, coef->len + 1);
  }
  return coef;
}

static inline void fillDC(block_t* blk, macroblock_t* mbk, stream* stm, const dc_coef_t* coef, int32_t* past) {
  int32_t val;
  if (coef->size) {
    val = stmGet32(stm, coef->size);
    if (!(val & (1 << (coef->size - 1)))) {
      val = (-1 << coef->size) | (val + 1);
    }
  } else {
    val = 0;
  }
  val <<= 3;
  if (mbk->macroblock_address - mbk->past_intra_address > 1) {
    mbk->past_intra_address = mbk->macroblock_address;
    blk->y_past = blk->cb_past = blk->cr_past = 0;
    // val += 1024;
  } else {
    val += *past;
  }
  *past = val;
  blk->raw[0] = val;
}

void blockIntraY(block_t* blk, macroblock_t* mbk, mpg_t* mpg, stream* stm) {
  const dc_coef_t* coef = checkDClum(stm);
  fillDC(blk, mbk, stm, coef, &blk->y_past);
}

void blockIntraCb(block_t* blk, macroblock_t* mbk, mpg_t* mpg, stream* stm) {
  const dc_coef_t* coef = checkDCchrom(stm);
  fillDC(blk, mbk, stm, coef, &blk->cb_past);
}

void blockIntraCr(block_t* blk, macroblock_t* mbk, mpg_t* mpg, stream* stm) {
  const dc_coef_t* coef = checkDCchrom(stm);
  fillDC(blk, mbk, stm, coef, &blk->cr_past);
}

void blockRLC(block_t* blk, macroblock_t* mbk, mpg_t* mpg, stream* stm, uint8_t flag) {
  uint16_t peep;
  int8_t   i = 0, non_intra_flag = flag;
  const ac_coef_t* coef;

  while (i < 64) {
    peep = stmPeep32(stm, 16);

    if (peep >= 0x2800) {
      if (flag) {
        coef = DCT_5_DC + stmPeep32(stm, 5) - 5;
        --i;
      } else {
        coef = DCT_5_AC + stmPeep32(stm, 5) - 5;
      }
    } else if (peep >= 0x0400) {
      coef = DCT_8 + stmPeep32(stm, 8) - 4;
    } else if (peep >= 0x0200) {
      coef = DCT_10 + stmPeep32(stm, 10) - 8;
    } else if (peep >= 0x0080) {
      coef = DCT_13 + stmPeep32(stm, 13) - 16;
    } else if (peep >= 0x0020) {
      coef = DCT_15 + stmPeep32(stm, 15) - 16;
    } else {
      coef = DCT_16 + peep;
      stmGet32(stm, 16);
    }

    flag = 0;
    int32_t val;

    if (coef->run == 65) {
      stmGet32(stm, 6);
      i += stmPeep32(stm, 6) + 1;
      stmGet32(stm, 6);
      uint8_t first = stmGet32(stm, 8);

      if (first == 0x80) {
        val = -256 + stmGet32(stm, 8);
      } else if (first == 0x00) {
        val = stmGet32(stm, 8);
      } else {
        if (first & 0x80) {
          val = -((int8_t) ~first + 1);
        } else {
          val = first;
        }
      }
      if (i >= 64) {
        // read EOB
        assert(stmGet32(stm, 2) == 2);
      }
    } else {
      stmGet32(stm, coef->len);
      i += coef->run;
      if (coef->run == 129 && coef->len == 2) {
        break;
      }
      val = coef->level;
      if (stmGet32(stm, 1)) {
        val = -val;
      }
      if (i >= 64) {
        // read EOB
        assert(stmGet32(stm, 2) == 2);
      }
    }
    if (non_intra_flag) {
      // fprintf(stderr, "non intra\n");
      val = ((2 * val + SIGN(val)) * mbk->quantizer_scale * mpg->non_intra_quant[i]) / 16;
    } else {
      val = (val * mbk->quantizer_scale * mpg->intra_quant[i]) / 8;
    }
    val = (val - 1) | 1;
    val = CLIP(val);
    if (i < 64) {
      blk->raw[ZIGZAG[i]] = val;
    }
  }
}

void mpgInit(mpg_t* mpg, FILE* fp) {
  int i;

  stmInit(&mpg->stm, fp);
  seqInit(&mpg->seq, &mpg->stm);

  mpg->seq.load_intra_quantizer_matrix = stmGet32(&mpg->stm, 1);
  if (mpg->seq.load_intra_quantizer_matrix) {
    mpg->intra_quant = (uint8_t*)malloc(64);
    for (i = 0; i < 64; ++i) {
      mpg->intra_quant[i] = stmAln8(&mpg->stm);
    }
  } else {
    mpg->intra_quant = INTRA_Q;
  }
  mpg->seq.load_non_intra_quantizer_matrix = stmGet32(&mpg->stm, 1);
  if (mpg->seq.load_non_intra_quantizer_matrix) {
    mpg->non_intra_quant = (uint8_t*)malloc(64);
    for (i = 0; i < 64; ++i) {
      mpg->non_intra_quant[i] = stmAln8(&mpg->stm);
    }
  } else {
    mpg->non_intra_quant = NON_INTRA_Q;
  }
  mpg->mb_width = mpg->seq.horizontal_size >> 4;
}

void mpgFinal(mpg_t* mpg) {
  if (mpg->seq.load_intra_quantizer_matrix) {
    free(mpg->intra_quant);
  }
  if (mpg->seq.load_non_intra_quantizer_matrix) {
    free(mpg->non_intra_quant);
  }
}
