#ifndef __MPGDEC_H__
#define __MPGDEC_H__

#include <stdint.h>
#include "bitstream.h"
#include "framebuf.h"

typedef struct _sequence_t {
  uint32_t sequence_header_code;
  uint16_t horizontal_size;
  uint16_t vertical_size;
  uint8_t  pel_aspect_ratio;
  uint8_t  picture_rate;
  uint32_t bit_rate;
  uint8_t  marker_bit;
  uint16_t vbv_buffer_size;
  uint8_t  constrained_parameter_flag;
  uint8_t  load_intra_quantizer_matrix;
  uint8_t  load_non_intra_quantizer_matrix;
} sequence_t;

typedef struct _gop_t {
  uint32_t group_start_code;
  uint32_t time_code;
  uint8_t  closed_gop;
  uint8_t  broken_link;
} gop_t;

#define IFRAME 1
#define PFRAME 2
#define BFRAME 3

typedef struct _picture_t {
  uint32_t picture_start_code;
  uint16_t temporal_reference;
  uint8_t  picture_coding_type;
  uint16_t vbv_delay;

  uint8_t  full_pel_forward_vector;
  uint8_t  forward_f_code;
  uint8_t  full_pel_backward_vector;
  uint8_t  backward_f_code;

  uint8_t  forward_r_size;
  uint8_t  forward_f;
  uint8_t  backward_r_size;
  uint8_t  backward_f;
} picture_t;

typedef struct _slice_t {
  uint32_t slice_start_code;
  uint8_t  quantizer_scale;
} slice_t;

typedef struct _macroblock_t {
  uint32_t macroblock_address_increment;
  uint8_t macroblock_type;
  uint8_t macroblock_quant;
  uint8_t macroblock_motion_forward;
  uint8_t macroblock_motion_backward;
  uint8_t macroblock_pattern;
  uint8_t macroblock_intra;
  uint8_t quantizer_scale;

  int8_t motion_horizontal_forward_code;
  uint8_t motion_horizontal_forward_r;
  int8_t motion_vertical_forward_code;
  uint8_t motion_vertical_forward_r;
  int8_t motion_horizontal_backward_code;
  uint8_t motion_horizontal_backward_r;
  int8_t motion_vertical_backward_code;
  uint8_t motion_vertical_backward_r;
  uint8_t coded_block_pattern;
  uint8_t pattern_code[6];

  int32_t macroblock_address;
  int32_t previous_macroblock_address;
  int32_t past_intra_address;
  uint8_t past_macroblock_intra;

  int32_t recon_right_for, recon_down_for;
  int32_t recon_right_back, recon_down_back;
  int32_t recon_right_for_prev, recon_down_for_prev;
  int32_t recon_right_back_prev, recon_down_back_prev;
} macroblock_t;

typedef struct _block_t {
  int32_t raw[64];
  int32_t y_past, cb_past, cr_past;
} block_t;

typedef struct _mpg_t {
  stream stm;
  sequence_t seq;
  uint8_t* intra_quant;
  uint8_t* non_intra_quant;
  uint32_t mb_width;

  frame_buffer* ref1;
  frame_buffer* ref2;
} mpg_t;

#define DCT_FIRST 1
#define DCT_NEXT  0

void seqInit(sequence_t*, stream*);
void gopInit(gop_t*, stream*);
void pictureInit(picture_t*, stream*);
void sliceInit(slice_t*, stream*);
void macroblockInit(macroblock_t*, picture_t*, slice_t*, stream*);
void blockMV(macroblock_t*, picture_t*);
void blockIntraY(block_t*, macroblock_t*, mpg_t*, stream*);
void blockIntraCb(block_t*, macroblock_t*, mpg_t*, stream*);
void blockIntraCr(block_t*, macroblock_t*, mpg_t*, stream*);
void blockRLC(block_t*, macroblock_t*, mpg_t*, stream*, uint8_t);
void mpgInit(mpg_t*, FILE*);
void mpgDecode(mpg_t*);
void mpgFinal(mpg_t*);

#endif
