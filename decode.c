#include <string.h>
#include <assert.h>
#include "mpgdec.h"
#include "framebuf.h"

#define FIX(x) (((x) < 0) ? 0 : (((x) > 0xFF) ? 0xFF : (uint8_t) (x)))
#define SFIX(x) (((x) < -256) ? -256 : (((x) > 255) ? 255 : (x)))

#define W1 2841
#define W2 2676
#define W3 2408
#define W5 1609
#define W6 1108
#define W7 565

void decodeRowIDCT(int32_t* raw, int32_t row) {
  int32_t x0, x1, x2, x3, x4, x5, x6, x7, x8;
  int32_t *blk = &raw[row << 3];

  // shortcut when AC terms are 0
  if (!((x1 = blk[4] << 11)
      | (x2 = blk[6])
      | (x3 = blk[2])
      | (x4 = blk[1])
      | (x5 = blk[7])
      | (x6 = blk[5])
      | (x7 = blk[3]))) {
    blk[0] = blk[1] = blk[2] = blk[3] = blk[4] = blk[5] = blk[6] = blk[7] = blk[0] << 3;
    return;
  }
  // AC term
  x0 = (blk[0] << 11) + 128;
  x8 = W7 * (x4 + x5);
  x4 = x8 + (W1 - W7) * x4;
  x5 = x8 - (W1 + W7) * x5;
  x8 = W3 * (x6 + x7);
  x6 = x8 - (W3 - W5) * x6;
  x7 = x8 - (W3 + W5) * x7;
  x8 = x0 + x1;
  x0 -= x1;
  x1 = W6 * (x3 + x2);
  x2 = x1 - (W2 + W6) * x2;
  x3 = x1 + (W2 - W6) * x3;
  x1 = x4 + x6;
  x4 -= x6;
  x6 = x5 + x7;
  x5 -= x7;
  x7 = x8 + x3;
  x8 -= x3;
  x3 = x0 + x2;
  x0 -= x2;
  x2 = (181 * (x4 + x5) + 128) >> 8;
  x4 = (181 * (x4 - x5) + 128) >> 8;
  // writing buckets
  blk[0] = (x7 + x1) >> 8;
  blk[1] = (x3 + x2) >> 8;
  blk[2] = (x0 + x4) >> 8;
  blk[3] = (x8 + x6) >> 8;
  blk[4] = (x8 - x6) >> 8;
  blk[5] = (x0 - x4) >> 8;
  blk[6] = (x3 - x2) >> 8;
  blk[7] = (x7 - x1) >> 8;
}

void decodeDiffColumnIDCT(int32_t* raw, int32_t column, int32_t* map) {
  int32_t x0, x1, x2, x3, x4, x5, x6, x7, x8;
  int32_t* blk = &raw[column];

  // shortcut when AC terms are 0
  if (!((x1 = blk[8*4] << 8)
      | (x2 = blk[8*6])
      | (x3 = blk[8*2])
      | (x4 = blk[8*1])
      | (x5 = blk[8*7])
      | (x6 = blk[8*5])
      | (x7 = blk[8*3]))) {
     map[8*0]= map[8*1]= map[8*2]= map[8*3]= map[8*4]= map[8*5]= map[8*6]= map[8*7] = SFIX((blk[8*0]+32)>>6);
    return;
  }
  x0 = (blk[0] << 8) + 8192;
  x8 = W7 * (x4 + x5) + 4;
  x4 = (x8 + (W1 - W7) * x4) >> 3;
  x5 = (x8 - (W1 + W7) * x5) >> 3;
  x8 = W3 * (x6 + x7) + 4;
  x6 = (x8 - (W3 - W5) * x6) >> 3;
  x7 = (x8 - (W3 + W5) * x7) >> 3;
  x8 = x0 + x1;
  x0 -= x1;
  x1 = W6 * (x3 + x2) + 4;
  x2 = (x1 - (W2 + W6) * x2) >> 3;
  x3 = (x1 + (W2 - W6) * x3) >> 3;
  x1 = x4 + x6;
  x4 -= x6;
  x6 = x5 + x7;
  x5 -= x7;
  x7 = x8 + x3;
  x8 -= x3;
  x3 = x0 + x2;
  x0 -= x2;
  x2 = (181 * (x4 + x5) + 128) >> 8;
  x4 = (181 * (x4 - x5) + 128) >> 8;
  // writing output
  *map = SFIX((x7 + x1) >> 14);  map += 8;
  *map = SFIX((x3 + x2) >> 14);  map += 8;
  *map = SFIX((x0 + x4) >> 14);  map += 8;
  *map = SFIX((x8 + x6) >> 14);  map += 8;
  *map = SFIX((x8 - x6) >> 14);  map += 8;
  *map = SFIX((x0 - x4) >> 14);  map += 8;
  *map = SFIX((x3 - x2) >> 14);  map += 8;
  *map = SFIX((x7 - x1) >> 14);
}

void decodeColumnIDCT(int32_t* raw, int32_t column, int32_t column_size, uint8_t* map) {
  int32_t x0, x1, x2, x3, x4, x5, x6, x7, x8;
  int32_t* blk = &raw[column];

  // shortcut when AC terms are 0
  if (!((x1 = blk[8*4] << 8)
      | (x2 = blk[8*6])
      | (x3 = blk[8*2])
      | (x4 = blk[8*1])
      | (x5 = blk[8*7])
      | (x6 = blk[8*5])
      | (x7 = blk[8*3]))) {
    x1 = FIX(((blk[0] + 32) >> 6) + 128);
    // writing output
    for (x0 = 8; x0 > 0; --x0) {
      *map = (uint8_t) x1;
      map += column_size;
    }
    return;
  }
  x0 = (blk[0] << 8) + 8192;
  x8 = W7 * (x4 + x5) + 4;
  x4 = (x8 + (W1 - W7) * x4) >> 3;
  x5 = (x8 - (W1 + W7) * x5) >> 3;
  x8 = W3 * (x6 + x7) + 4;
  x6 = (x8 - (W3 - W5) * x6) >> 3;
  x7 = (x8 - (W3 + W5) * x7) >> 3;
  x8 = x0 + x1;
  x0 -= x1;
  x1 = W6 * (x3 + x2) + 4;
  x2 = (x1 - (W2 + W6) * x2) >> 3;
  x3 = (x1 + (W2 - W6) * x3) >> 3;
  x1 = x4 + x6;
  x4 -= x6;
  x6 = x5 + x7;
  x5 -= x7;
  x7 = x8 + x3;
  x8 -= x3;
  x3 = x0 + x2;
  x0 -= x2;
  x2 = (181 * (x4 + x5) + 128) >> 8;
  x4 = (181 * (x4 - x5) + 128) >> 8;
  // writing output
  *map = FIX(((x7 + x1) >> 14) + 128);  map += column_size;
  *map = FIX(((x3 + x2) >> 14) + 128);  map += column_size;
  *map = FIX(((x0 + x4) >> 14) + 128);  map += column_size;
  *map = FIX(((x8 + x6) >> 14) + 128);  map += column_size;
  *map = FIX(((x8 - x6) >> 14) + 128);  map += column_size;
  *map = FIX(((x0 - x4) >> 14) + 128);  map += column_size;
  *map = FIX(((x3 - x2) >> 14) + 128);  map += column_size;
  *map = FIX(((x7 - x1) >> 14) + 128);
}

static inline void blkDiffIDCT(block_t* blk, int32_t* dest) {
  uint8_t i;

  for (i = 0; i < 8; ++i) {
    decodeRowIDCT(blk->raw, i);
  }
  for (i = 0; i < 8; ++i) {
    decodeDiffColumnIDCT(blk->raw, i, dest + i);
  }
}

static inline void blkIDCT(block_t* blk, uint8_t* buf, uint32_t width, uint32_t offset) {
  uint8_t i;

  for (i = 0; i < 8; ++i) {
    decodeRowIDCT(blk->raw, i);
  }
  for (i = 0; i < 8; ++i) {
    decodeColumnIDCT(blk->raw, i, width, buf + offset + i);
  }
}

#define Y_BLOCK 0
#define C_BLOCK 1
#define ARY(a,x,y) (*(a + (offset + (y) * width + (x))))
#define OARY(a,x,y) (a[((y) << 3) + (x)])

static inline void motionComp(const uint8_t* from_old, const int32_t* from_new, uint8_t*  to, int32_t recon_right, int32_t recon_down, int32_t offset, int32_t width, uint8_t bflag) {
  int32_t right, down, right_half, down_half;
  right = (recon_right >> bflag) >> 1;
  down = (recon_down >> bflag) >> 1;
  right_half = (recon_right >> bflag) - (right << 1);
  down_half = (recon_down >> bflag) - (down << 1);

  int32_t i, j, a, b, c, d;

  if (!right_half && !down_half) {
    for (j = 0; j < 8; ++j) {
      for (i = 0; i < 8; ++i) {
        a = ARY(from_old, i + right, j + down);
        ARY(to, i, j) = FIX(a + OARY(from_new, i, j));
        // fprintf(stderr, "%d %d %d\n", ARY(to, i, j), a, OARY(from_new, i, j));
      }
    }
  } else if (!right_half && down_half) {
    for (j = 0; j < 8; ++j) {
      for (i = 0; i < 8; ++i) {
        a = ARY(from_old, i + right, j + down);
        b = ARY(from_old, i + right, j + down + 1);
        ARY(to, i, j) = FIX(((a + b) >> 1) + OARY(from_new, i, j));
      }
    }
  } else if (right_half && !down_half) {
    for (j = 0; j < 8; ++j) {
      for (i = 0; i < 8; ++i) {
        a = ARY(from_old, i + right, j + down);
        b = ARY(from_old, i + right + 1, j + down);
        ARY(to, i, j) = FIX(((a + b) >> 1) + OARY(from_new, i, j));
      }
    }
  } else if (right_half && down_half) {
    for (j = 0; j < 8; ++j) {
      for (i = 0; i < 8; ++i) {
        a = ARY(from_old, i + right, j + down);
        b = ARY(from_old, i + right + 1, j + down);
        c = ARY(from_old, i + right, j + down + 1);
        d = ARY(from_old, i + right + 1, j + down + 1);
        ARY(to, i, j) = FIX(((a + b + c + d) >> 2) + OARY(from_new, i, j));
      }
    }
  }
}

#undef ARY
#undef OARY

static inline void blockAddPrev(macroblock_t* mbk, picture_t* pic, const uint8_t* ref1, const uint8_t* ref2, int32_t* from_new, uint8_t* to, uint32_t width, uint32_t offset, uint8_t bflag) {

  uint8_t i, j;
  static uint8_t buf[64];

  if (mbk->macroblock_motion_forward && mbk->macroblock_motion_backward) {
    motionComp(ref2, from_new, to, mbk->recon_right_for, mbk->recon_down_for, offset, width, bflag);
    for (j = 0; j < 8; ++j) {
      for (i = 0; i < 8; ++i) {
        buf[(j << 3) + i] = *(to + offset + (j * width) + i);
      }
    }
    motionComp(ref1, from_new, to, mbk->recon_right_back, mbk->recon_down_back, offset, width, bflag);
    for (j = 0; j < 8; ++j) {
      for (i = 0; i < 8; ++i) {
        *(to + offset + (j * width) + i) = (*(to + offset + (j * width) + i) >> 1) + (buf[(j << 3) + i] >> 1);
      }
    }
  } else if (mbk->macroblock_motion_forward) {
    if (pic->picture_coding_type == BFRAME) {
      motionComp(ref2, from_new, to, mbk->recon_right_for, mbk->recon_down_for, offset, width, bflag);
    } else {
      motionComp(ref1, from_new, to, mbk->recon_right_for, mbk->recon_down_for, offset, width, bflag);
    }
  } else if (mbk->macroblock_motion_backward) {
    if (pic->picture_coding_type == BFRAME) {
      motionComp(ref1, from_new, to, mbk->recon_right_back, mbk->recon_down_back, offset, width, bflag);
    } else {
      motionComp(ref2, from_new, to, mbk->recon_right_back, mbk->recon_down_back, offset, width, bflag);
    }
  } else {
    // FIXME: maybe not correct
    motionComp(ref1, from_new, to, mbk->recon_right_for, mbk->recon_down_for, offset, width, bflag);
  }
}

static const uint8_t Y_X[] = {0, 1, 0, 1};
static const uint8_t Y_Y[] = {0, 0, 1, 1};

#define Y_OFFSET ((((y<<1) + Y_Y[i])<<3) * width + (((x<<1) + Y_X[i])<<3))
#define C_OFFSET ((y<<3) * (width >> 1) + (x<<3))

static int32_t add_buf[64];

static inline void fillSkippedBlock(macroblock_t* mbk, picture_t* pic, mpg_t* mpg, frame_buffer* fb, uint32_t prev, uint32_t now) {
  // TODO: B-frame
  uint8_t i;
  uint32_t x, y, width;

  memset(add_buf, 0, 64 * sizeof(int32_t));
  width = mpg->seq.horizontal_size;
  for (++prev; prev < now; ++prev) {
    x = prev % mpg->mb_width;
    y = prev / mpg->mb_width;

    for (i = 0; i < 4; ++i) {
      blockAddPrev(mbk, pic, mpg->ref1->cy, ((mpg->ref2) ? mpg->ref2->cy : NULL),add_buf, fb->cy, width, Y_OFFSET, Y_BLOCK);
    }
    blockAddPrev(mbk, pic, mpg->ref1->cb, ((mpg->ref2) ? mpg->ref2->cb : NULL), add_buf, fb->cb, width >> 1, C_OFFSET, C_BLOCK);
    blockAddPrev(mbk, pic, mpg->ref1->cr, ((mpg->ref2) ? mpg->ref2->cr : NULL), add_buf, fb->cr, width >> 1, C_OFFSET, C_BLOCK);
  }
}

static inline void blkDecode(macroblock_t* mbk, picture_t* pic, block_t* blk, mpg_t* mpg, stream* stm, frame_buffer* fb) {
  uint8_t i;
  uint32_t x, y, width, offset;

  width = mpg->seq.horizontal_size;
  x = mbk->macroblock_address % mpg->mb_width;
  y = mbk->macroblock_address / mpg->mb_width;

  if (mbk->macroblock_intra) {
    for (i = 0; i < 4; ++i) {
      memset(&blk->raw, 0, 64 * sizeof(int32_t));
      blockIntraY(blk, mbk, mpg, stm);
      blockRLC(blk, mbk, mpg, stm, DCT_NEXT);
      offset = Y_OFFSET;
      blkIDCT(blk, fb->cy, width, offset);
    }
    offset = C_OFFSET;
    // Cb
    memset(&blk->raw, 0, 64 * sizeof(int32_t));
    blockIntraCb(blk, mbk, mpg, stm);
    blockRLC(blk, mbk, mpg, stm, DCT_NEXT);
    blkIDCT(blk, fb->cb, width >> 1, offset);
    // Cr
    memset(&blk->raw, 0, 64 * sizeof(int32_t));
    blockIntraCr(blk, mbk, mpg, stm);
    blockRLC(blk, mbk, mpg, stm, DCT_NEXT);
    blkIDCT(blk, fb->cr, width >> 1, offset);
    // fprintf(stderr, "cr: %d: %d, %d\n", width >> 1, x, y);
  } else if (mbk->macroblock_pattern) {
    for (i = 0; i < 6; ++i) {
      if (mbk->pattern_code[i]) {
        memset(&blk->raw, 0, 64 * sizeof(int32_t));
        blockRLC(blk, mbk, mpg, stm, DCT_FIRST);
        blkDiffIDCT(blk, add_buf);

        if (i < 4) {
          offset = Y_OFFSET;
          blockAddPrev(mbk, pic, mpg->ref1->cy, ((mpg->ref2) ? mpg->ref2->cy : NULL), add_buf, fb->cy, width, offset, Y_BLOCK);
        } else {
          offset = C_OFFSET;
          if (i == 4) {
            blockAddPrev(mbk, pic, mpg->ref1->cb, ((mpg->ref2) ? mpg->ref2->cb : NULL), add_buf, fb->cb, width >> 1, offset, C_BLOCK);
          } else {
            blockAddPrev(mbk, pic, mpg->ref1->cr, ((mpg->ref2) ? mpg->ref2->cr : NULL), add_buf, fb->cr, width >> 1, offset, C_BLOCK);
          }
        }
      } else {
        memset(add_buf, 0, 64 * sizeof(int32_t));
        if (i < 4) {
          blockAddPrev(mbk, pic, mpg->ref1->cy, ((mpg->ref2) ? mpg->ref2->cy : NULL),add_buf, fb->cy, width, Y_OFFSET, Y_BLOCK);
        } else if (i == 4) {
          blockAddPrev(mbk, pic, mpg->ref1->cb, ((mpg->ref2) ? mpg->ref2->cb : NULL), add_buf, fb->cb, width >> 1, C_OFFSET, C_BLOCK);
        } else {
          blockAddPrev(mbk, pic, mpg->ref1->cr, ((mpg->ref2) ? mpg->ref2->cr : NULL), add_buf, fb->cr, width >> 1, C_OFFSET, C_BLOCK);
        }
      }
    }
  } else {
    fillSkippedBlock(mbk, pic, mpg, fb, mbk->macroblock_address - 1, mbk->macroblock_address + 1);
  }
}

#undef Y_OFFSET
#undef C_OFFSET

inline void mpgAddRef(mpg_t* mpg, frame_buffer* fb) {
  mpg->ref2 = mpg->ref1;
  mpg->ref1 = fb;
}

void mpgDecode(mpg_t* mpg) {
  gop_t gop;
  picture_t pic;
  slice_t slc;
  macroblock_t mbk;
  block_t blk;
  frame_buffer* fb;

  uint32_t slice_height, gop_count = 0, pic_count = 0;

  mpg->ref1 = NULL;
  mpg->ref2 = NULL;
  do {
    gopInit(&gop, &mpg->stm);
    dspClearGop();

    gop_count++;

    do {
      pictureInit(&pic, &mpg->stm);
      pic_count++;
      fprintf(stderr, "pic: %d\n", pic_count);
      switch(pic.picture_coding_type) {
        case IFRAME:
          fprintf(stderr, "IFRAME\n"); break;
        case PFRAME:
          fprintf(stderr, "PFRAME\n"); break;
        case BFRAME:
          fprintf(stderr, "BFRAME\n"); break;
      }
      fb = frmGetFrame(mpg->seq.horizontal_size, mpg->seq.vertical_size);
      fb->order = pic.temporal_reference;
      do {
        // fprintf(stderr, "new slice\n");
        sliceInit(&slc, &mpg->stm);
        slice_height = slc.slice_start_code & 0xFF - 1;
        mbk.previous_macroblock_address = slice_height * (mpg->seq.horizontal_size >> 3) - 1;
        mbk.past_intra_address = -2;
        mbk.macroblock_address = -1;
        mbk.recon_right_for_prev = mbk.recon_down_for_prev = 0;
        mbk.recon_right_back_prev = mbk.recon_down_back_prev = 0;
        blk.y_past = 0;
        blk.cb_past = 0;
        blk.cr_past = 0;
        do {
          macroblockInit(&mbk, &pic, &slc, &mpg->stm);
          if (mbk.macroblock_address - mbk.previous_macroblock_address > 1) {
            if (pic.picture_coding_type == PFRAME) {
              mbk.recon_right_for_prev = mbk.recon_down_for_prev = 0;
              mbk.recon_right_back_prev = mbk.recon_down_back_prev = 0;
            }
            fillSkippedBlock(&mbk, &pic, mpg, fb, mbk.previous_macroblock_address, mbk.macroblock_address);
          }
          if (pic.picture_coding_type == PFRAME && !mbk.macroblock_motion_forward) {
            mbk.recon_right_for_prev = mbk.recon_down_for_prev = 0;
          }
          if (pic.picture_coding_type == BFRAME && mbk.macroblock_intra) {
            mbk.recon_right_for_prev = mbk.recon_down_for_prev = 0;
            mbk.recon_right_back_prev = mbk.recon_down_back_prev = 0;
          }
          blockMV(&mbk, &pic);
          blkDecode(&mbk, &pic, &blk, mpg, &mpg->stm, fb);
        } while (stmPeep32(&mpg->stm, 18) != 0);
        stmNextStartCode(&mpg->stm);
      } while (stmPeep32(&mpg->stm, 32) == 0x101);
      fprintf(stderr, "pushing pic: %d -> %d\n", pic_count, pic.temporal_reference);
      dspShow(fb);
      if (pic.picture_coding_type == IFRAME || pic.picture_coding_type == PFRAME) {
        mpgAddRef(mpg, fb);
      }
    } while (stmPeep32(&mpg->stm, 32) == 0x100);
    frmFreeAll();
  } while (stmPeep32(&mpg->stm, 32) == 0x1B8);
}
