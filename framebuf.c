#include <stdio.h>
#include <stdlib.h>
#include "framebuf.h"

static frame_buffer* availFb = NULL;
static frame_buffer* usedFb = NULL;

static inline void frameInit(frame_buffer* fb, uint16_t width, uint16_t height) {
  uint32_t size = width * height;
  fb->cy = (uint8_t*) malloc(size);
  fb->cb = (uint8_t*) malloc(size >> 2);
  fb->cr = (uint8_t*) malloc(size >> 2);
}

static inline void frameFinal(frame_buffer* fb) {
  free(fb->cy);
  free(fb->cb);
  free(fb->cr);
}

frame_buffer* frmGetFrame(uint16_t width, uint16_t height) {
  frame_buffer* fb;
  if (availFb == NULL) {
    fb = (frame_buffer*) malloc(sizeof(frame_buffer));
    frameInit(fb, width, height);
  } else {
    fb = availFb;
    availFb = fb->next;
    if (fb->next) {
      fb->next->prev = NULL;
    }
  }
  fb->next = usedFb;
  fb->prev = NULL;
  usedFb = fb;
  return fb;
}

void frmFreeFrame(frame_buffer* fb) {
  if (fb->prev) {
    fb->prev->next = fb->next;
  } else {
    usedFb = NULL;
  }
  if (fb->next) {
    fb->next->prev = fb->prev;
  }
  fb->next = availFb;
  fb->prev = NULL;
  availFb = fb;
}

void frmFreeAll() {
  frame_buffer* fb = usedFb;
  while (fb != NULL) {
    frame_buffer* next = fb->next;
    frmFreeFrame(fb);
    fb = next;
  }
}

void frmFinal() {
  frame_buffer* fb = availFb;
  while (fb != NULL) {
    frame_buffer* next = fb->next;
    frameFinal(fb);
    free(fb);
    fb = next;
  }
}
