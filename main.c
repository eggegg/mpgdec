#include <stdio.h>
#include <stdlib.h>
#include "mpgdec.h"
#include "framebuf.h"

int main(int argc, char **argv) {
  FILE* video = fopen(argv[1], "rb");

  mpg_t mpg;
  mpgInit(&mpg, video);
  dspInit(mpg.seq.horizontal_size, mpg.seq.vertical_size);
  mpgDecode(&mpg);
  mpgFinal(&mpg);
  dspFinal();

  return 0;
}
